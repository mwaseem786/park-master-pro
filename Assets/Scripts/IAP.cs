﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class IAP : MonoBehaviour
{
    public string[] items;

    private const int itemNameIndex = 3;
    private const int itemValueIndex = 4;

    private string itemName;
    private int itemValue;

    public void OnPurchaseComplete(Product product)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (product.definition.id == items[i])
            {
                string[] currentItem = items[i].Split('.');
                itemName = currentItem[itemNameIndex];
                itemValue = int.Parse(currentItem[itemValueIndex]);
                if(itemName == "removeads")
                {
                    Debug.Log("Ads removed successfully");
                    PlayerPrefs.SetInt("AdsPurchased", 1);
                    //GoogleAdsScript.Instance.HideBanner();
                    //UnityAdsScript.Instance.HideBanner();
                    if (int.Parse(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name) == 0)
                    {
                        GameObject.Find("Ads").GetComponent<Button>().interactable = false;
                    }
                    else
                        GameCanvasManager.instance.DisableAdButtons();
                }

            }
        }
    }

    public void OnpurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(product.definition.id + "failed because " + failureReason);
    }
}
