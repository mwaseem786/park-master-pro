﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Variables

    public static GameManager Instance;

    public bool isGameStarted;

    //int totalCoin;
    int currentCoin;

    public int currentLevelCoins=0;
    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        //PlayerPrefs.SetInt("totalStars", 1500);

        /*if (PlayerPrefs.GetInt("UnlockedLevel") == 0)
            PlayerPrefs.SetInt("UnlockedLevel", 1);*/

        if (PlayerPrefs.GetInt("currentCar") == 0)
            PlayerPrefs.SetInt("currentCar", 1);

        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
        //DontDestroyOnLoad(gameObject);

        Instantiate(Resources.Load("Prefabs/CarSpawner", typeof(GameObject)) as GameObject);
        Instantiate(Resources.Load("Prefabs/BeforeTouch", typeof(GameObject)) as GameObject);
        Instantiate(Resources.Load("Prefabs/Sounds", typeof(GameObject)) as GameObject);


        if (PlayerPrefs.GetInt("AdsPurchased") == 0)
        {
            //AdsScript.Instance.ShowBanner();
        }
        

    }

    #endregion

    #region Other Methods
    public void StartGame()
    {
        isGameStarted = true;
        UIManager.Instance.StartGame();
    }

    public void GetCoin()
    {
        currentCoin++;
        currentLevelCoins += 1;
        Debug.Log("Coins collected in this level: "+ currentLevelCoins);
        UIManager.Instance.UpdateCoinText(currentCoin);
    }

    public void SaveCoin()
    {
        PlayerPrefs.SetInt("totalStars", currentCoin);
    }

    public void RewardFor2x()
    {
        currentLevelCoins *= 2;
        Debug.Log("Coins collected in this level : " + currentLevelCoins);
        PlayerPrefs.SetInt("totalStars", currentCoin + currentLevelCoins);

        UIManager.Instance.UpdateCurrentCoinsText();
        //UIManager.Instance.UpdateCoinText(PlayerPrefs.GetInt("totalStars"));
    }

    public void LoadNewLevel()
    {
        isGameStarted = false;
        //currentCoin = totalCoin;
        //UIManager.Instance.UpdateCoinText(totalCoin);
        currentCoin = PlayerPrefs.GetInt("totalStars");
        UIManager.Instance.UpdateCoinText(currentCoin);
    }

    #endregion
}
