﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class MenuManager : MonoBehaviour
{
    private Text starsText;
    public Slider soundSlider;

    private void Awake()
    {
        if(PlayerPrefs.GetInt("firstTime",0) == 0)
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Screenshots/");
            Directory.CreateDirectory(Application.persistentDataPath + "/DataFiles/");

            var levelFile = Resources.Load<TextAsset>("Files/Levels");
            File.WriteAllText(Application.persistentDataPath + "/DataFiles/Levels.txt", levelFile.ToString());
            
            var carFile = Resources.Load<TextAsset>("Files/Cars");
            File.WriteAllText(Application.persistentDataPath + "/DataFiles/Cars.txt", carFile.ToString());

            /*Destroy(levelFile, true);
            Destroy(carFile);*/

            

            PlayerPrefs.SetInt("firstTime", 1);
            PlayerPrefs.SetInt("Vibration", 1);
            PlayerPrefs.SetInt("UnlockedCars", 1);

            PlayerPrefs.SetInt("UnlockedLevel", 1);
        }

        if(PlayerPrefs.GetInt("AdsPurchased")==1)
        {
            GameObject.Find("Ads").GetComponent<Button>().interactable = false;
        }

    }

    private void Start()
    {
        starsText = GameObject.Find("Text").GetComponent<Text>();
        starsText.text = PlayerPrefs.GetInt("totalStars",0).ToString();

        soundSlider.value = PlayerPrefs.GetFloat("SoundVolume", 1);

    }

    public void OnClickStart()
    {
        if(PlayerPrefs.GetInt("UnlockedLevel") == SceneManager.sceneCountInBuildSettings-1)
        {
            //SceneController.Instance.LoadLevel(1);
            SceneManager.LoadScene(1);
        }
        else
            SceneManager.LoadScene(PlayerPrefs.GetInt("UnlockedLevel"));

    }

    public void OpenURL(string URL)
    {
        //DracoArts.Instance.UtilityController.ShowAppRatingPopup();
        Application.OpenURL(URL);
        
    }

    public void OpenPanel(GameObject panel)
    {
        panel.SetActive(true);
    }
    public void ClosePanel(GameObject panel)
    {
        panel.SetActive(false);
    }

    public void ChangeValue()
    {
        PlayerPrefs.SetFloat("SoundVolume", soundSlider.value);
    }

}
