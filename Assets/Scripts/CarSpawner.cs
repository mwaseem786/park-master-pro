﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Linq;

public class CarSpawner : MonoBehaviour
{
    private GameObject[] spawnPoints;
    private int dataIndex = 0;

    private int levelNo;
    private int noOfCars;
    private char space = ' ';
    private char comma = ',';


    private string[] carColors;

    private string[] position;

    private Vector3[] carPos;
    private Vector3[] parkPos;
    private int[] carRotations;

    private int car;


    void Awake()
    {
        car = PlayerPrefs.GetInt("currentCar", 1);
        levelNo = int.Parse(SceneManager.GetActiveScene().name);


        string filedata = Application.persistentDataPath + "/DataFiles/Levels.txt";
        //string filedata = "Assets/Resources/TextAssets/Levels.json";

        string line = File.ReadLines(filedata).Skip(levelNo-1).Take(levelNo).First();
        string[] data = line.Split(space);

        noOfCars = int.Parse(data[dataIndex]);

        carColors = new string[noOfCars];
        carPos = new Vector3[noOfCars];
        parkPos = new Vector3[noOfCars];
        carRotations = new int[noOfCars];


        for(int i = 0;i<noOfCars;i++)
        {
            dataIndex += 1;
            carColors[i] = data[dataIndex];

            dataIndex += 1;
            position = data[dataIndex].Split(comma);
            Vector3 pos = new Vector3(float.Parse(position[0]), float.Parse(position[1]), float.Parse(position[2]));
            carPos[i] = pos;

            dataIndex += 1;
            carRotations[i] = int.Parse(data[dataIndex]);

            dataIndex += 1;
            position = data[dataIndex].Split(comma);
            pos = new Vector3(float.Parse(position[0]), float.Parse(position[1]), float.Parse(position[2]));
            parkPos[i] = pos;

            Instantiate(Resources.Load("Paths/" + carColors[i] + "PathCreator", typeof(GameObject)), GameObject.Find("PathCreators").transform);
            Instantiate(Resources.Load("Cars/" + car + carColors[i], typeof(GameObject)), carPos[i], Quaternion.Euler(0, carRotations[i], 0), GameObject.Find("Cars").transform);
            Instantiate(Resources.Load("Parks/" + carColors[i] + "Park", typeof(GameObject)), parkPos[i], Quaternion.identity, GameObject.Find("ParkingSigns").transform);
            
        }
    }
}
