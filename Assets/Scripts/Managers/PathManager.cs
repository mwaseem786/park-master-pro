﻿using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.SceneManagement;

public class PathManager : MonoBehaviour
{
    #region Variables

    public static PathManager Instance;

    [SerializeField] PathCreator[] paths;
    [SerializeField] Car[] cars;

    PathCreator currentPath;
    int index;
    //AfterTouchScript afterTouchScript;
    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        int levelNo = int.Parse(SceneManager.GetActiveScene().name);

        string filedata = Application.persistentDataPath + "/DataFiles/Levels.txt";

        //string filedata = "Assets/Resources/TextAssets/Levels.json";

        string line = File.ReadLines(filedata).Skip(levelNo - 1).Take(levelNo).First();
        string[] data = line.Split(' ');

        int noOfCars = int.Parse(data[0]);

        paths = new PathCreator[noOfCars];
        cars = new Car[noOfCars];

        //cars = GameObject.Find()
        GameObject[] tCars = GameObject.FindGameObjectsWithTag("Car");
        GameObject[] tpaths = GameObject.FindGameObjectsWithTag("PathCreator");
        for(int i = 0;i<noOfCars;i++)
        {
            cars[i] = tCars[i].GetComponent<Car>();
            paths[i] = tpaths[i].GetComponent<PathCreator>();
        }

        Instance = this;
        index = 0;
        //ActivePath(index);
    }

    #endregion

   /* private void Update()
    {
        foreach (PathCreator p in paths)
        {

            Debug.Log(p.name);
        }
    }*/

    public bool checkPaths()
    {
        foreach (PathCreator p in paths)
        {
            if (p.enabled == true)
            {
                return false;
            }
        }
        return true;
    }

    public void SKRA(string name)
    {
        int x = 0;
        /*string[] characters = new string[name.Length];
        for (int i = 0; i < name.Length; i++)
        {
            characters[i] = System.Convert.ToString(name[i]);
        }*/
        char character = name[1];
        foreach (PathCreator p in paths)
        {
            if (p.enabled == true)
            {
                return;
            } 
        }

        //Debug.Log(character);

        foreach (PathCreator p in paths)
        {
            if(p.name == (character + "PathCreator(Clone)"))
            {
                ActivePath(x);
                index = x;
            }
            x++;
        }
    }


    #region Other Methods

    public void NextPathDrawing()
    {
        currentPath.enabled = false;
        //cars[index].HideArrow();

        cars[index].StartToMoving();

        /*if (index < paths.Length - 1)
        {
            index++;
            ActivePath(index);
        }
        else
        {
            *//*foreach (Car car in cars)
            {
                car.StartToMoving();
            }*//*

            //otherCode
            
            afterTouchScript.enabled = true;

        }*/
    }

    void ActivePath(int index)
    {
        currentPath = paths[index];
        currentPath.enabled = true;
        //cars[index].ShowArrow();
    }

    #endregion
}
