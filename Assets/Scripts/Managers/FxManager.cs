﻿//using DG.Tweening;
using UnityEngine;

public class FxManager : MonoBehaviour
{
    #region Variables

    public static FxManager Instance;

    [SerializeField] new Camera camera;
    [SerializeField] GameObject crashFx;
    [SerializeField] GameObject levelPassedFx;
    [SerializeField] GameObject coinFx;
    [SerializeField] GameObject[] confetti;

    [SerializeField] GameObject blinker;

    [SerializeField] private GameObject[] carsObject;
    private Transform[] carsObjectTransform;

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        Instance = this;
        carsObject = GameObject.FindGameObjectsWithTag("Car");
    }

    #endregion

    #region Other Methods

    public void GetCoin(Vector3 pos)
    {
        GameObject fx = Instantiate(coinFx, pos, Quaternion.identity);
    }

    public void Crash(Vector3 pos)
    {
        //GameObject fx = Instantiate(crashFx, pos, Quaternion.identity);
        //camera.transform.DOShakePosition(2.5f, .5f, 10, 90, false, true);
    }

    public void LevelComplete()
    {
        levelPassedFx.SetActive(true);
        foreach(GameObject c in confetti)
        {
            c.SetActive(true);
        }
    }


    public void spawnBlinker(Transform transform, string name)
    {
        char character = name[1];
        if (GameObject.Find(character + "SpawnBlinker(Clone)") != null)
            return;
        GameObject instance = Instantiate(Resources.Load(character + "SpawnBlinker", typeof(GameObject)), new Vector3(transform.position.x, 1f, transform.position.z), Quaternion.Euler(0, 0, 0)) as GameObject;
        
        GameCanvasManager.instance.DestroyUI();
    }


    #endregion
}
