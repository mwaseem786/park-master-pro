﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour
{
    private AudioSource audioSource;
    private static SoundScript instance;
    public static SoundScript Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<SoundScript>();
            }
            return instance;
        }
    }
    public void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefs.GetFloat("SoundVolume");
    }

    public void PlaySound(string soundName)
    {
        AudioClip clip = Resources.Load("Audio/" + soundName, typeof(AudioClip)) as AudioClip;
        audioSource.clip = clip;
        audioSource.Play();
    }
}
