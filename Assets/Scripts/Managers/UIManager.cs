﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Variables

    public static UIManager Instance;

    //[SerializeField] GameObject infoScreen;
    [SerializeField] GameObject gameScreen;
    [SerializeField] GameObject nextLevelScreen;
    [SerializeField] Text[] coinsText;
    [SerializeField] Text levelText;


    public Text currentLevelStarsText;

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        Instance = this;
        levelText.text = "Level " + (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        PlayerPrefs.SetInt("StopInterstitialAfter2x", 0);
    }

    #endregion

    #region Other Methods

    public void UpdateCurrentCoinsText()
    {
        currentLevelStarsText.text = GameManager.Instance.currentLevelCoins.ToString();
    }

    public void StartGame()
    {
        //infoScreen.SetActive(false);
        gameScreen.SetActive(true);
    }

    public void LevelPassed()
    {
        nextLevelScreen.SetActive(true);
        if (int.Parse(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name) < 3)
        {
            GameObject.Find("WatchFor2x").gameObject.SetActive(false);
            GameObject.Find("CurrentLevelStars").gameObject.SetActive(false);
        }
    }

    public void UpdateCoinText(int coin)
    {
        currentLevelStarsText.text = GameManager.Instance.currentLevelCoins.ToString();
        foreach (Text ct in coinsText)
        {
            ct.text = coin.ToString();
        }
    }

    public void restart()
    {
        //DracoArts.Instance.promotionController.ShowPromotion();
        SceneController.Instance.RestartLevel();
       
    }
    
    public void next()
    {
        if(PlayerPrefs.GetInt("AdsPurchased")==1)
        {
            //DracoArts.Instance.promotionController.ShowPromotion();
            SceneController.Instance.NextLevel();
        }    
        else
        {
            if (int.Parse(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name) >= 30)
            {
                if(PlayerPrefs.GetInt("StopInterstitialAfter2x")==0)
                {
                    //AdsScript.Instance.ShowInterstitial();
                }
                else
                    SceneController.Instance.NextLevel();
            }
            else
                SceneController.Instance.NextLevel();
        }
            
    }

    public void load(int level)
    {
        SceneController.Instance.LoadLevel(level);
    }

    public void WatchFor2x()
    {
        //AdsScript.Instance.ShowRewarded();
        //AdsScript.Instance.FreeCurrencyFunction();
        GameObject.Find("WatchFor2x").gameObject.SetActive(false);
    }

    #endregion
}
